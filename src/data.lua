local noise = require("noise")
local tne = noise.to_noise_expression

local function make_autoplace_settings(value)
    return {
        default_enabled = false,
        probability_expression = tne(value)
    }
end

data:extend({
    {
        type = "noise-layer",
        name = "surf-orig-ground"
    },
    {
        type = "noise-layer",
        name = "surf-orig-wall"
    },
    {
        type = "tile",
        name = "surf-orig-ground",
        collision_mask = {"ground-tile"},
        autoplace = make_autoplace_settings(1),
        layer = 38,
        variants =
        {
            main =
            {
                {
                    picture = "__Surfaces_Original__/graphics/terrain/cave-ground/cave-ground1.png",
                    count = 16,
                    size = 1,
                    hr_version =
                    {
                        picture = "__Surfaces_Original__/graphics/terrain/cave-ground/hr-cave-ground1.png",
                        count = 16,
                        size = 1,
                        scale = 0.5,
                    },
                },
                {
                    picture = "__Surfaces_Original__/graphics/terrain/cave-ground/cave-ground2.png",
                    count = 16,
                    size = 2,
                    hr_version =
                    {
                        picture = "__Surfaces_Original__/graphics/terrain/cave-ground/hr-cave-ground2.png",
                        count = 16,
                        size = 2,
                        scale = 0.5,
                    },
                },
                {
                    picture = "__Surfaces_Original__/graphics/terrain/cave-ground/cave-ground4.png",
                    count = 16,
                    size = 4,
                    hr_version =
                    {
                        picture = "__Surfaces_Original__/graphics/terrain/cave-ground/hr-cave-ground4.png",
                        count = 16,
                        size = 4,
                        scale = 0.5,
                    }
                },
                {
                    picture = "__Surfaces_Original__/graphics/terrain/cave-ground/cave-ground8.png",
                    line_length = 4,
                    count = 16,
                    size = 8,
                    hr_version =
                    {
                        picture = "__Surfaces_Original__/graphics/terrain/cave-ground/hr-cave-ground8.png",
                        line_length = 4,
                        count = 16,
                        size = 8,
                        scale = 0.5,
                    }
                },
                {
                    picture = "__Surfaces_Original__/graphics/terrain/cave-ground/cave-ground16.png",
                    line_length = 4,
                    count = 16,
                    size = 16,
                    hr_version =
                    {
                        picture = "__Surfaces_Original__/graphics/terrain/cave-ground/hr-cave-ground16.png",
                        line_length = 4,
                        count = 16,
                        size = 16,
                        scale = 0.5,
                    }
                },
            },
            inner_corner =
            {
                picture = "__Surfaces_Original__/graphics/terrain/cave-ground/cave-ground-inner-corner.png",
                count = 8,
                hr_version =
                {
                    picture = "__Surfaces_Original__/graphics/terrain/cave-ground/hr-cave-ground-inner-corner.png",
                    count = 8,
                    scale = 0.5,
                },
            },
            outer_corner =
            {
                picture = "__Surfaces_Original__/graphics/terrain/cave-ground/cave-ground-outer-corner.png",
                count = 8,
                hr_version =
                {
                    picture = "__Surfaces_Original__/graphics/terrain/cave-ground/hr-cave-ground-outer-corner.png",
                    count = 8,
                    scale = 0.5,
                },
            },
            side =
            {
                picture = "__Surfaces_Original__/graphics/terrain/cave-ground/cave-ground-side.png",
                count = 8,
                hr_version =
                {
                    picture = "__Surfaces_Original__/graphics/terrain/cave-ground/hr-cave-ground-side.png",
                    count = 8,
                    scale = 0.5,
                },
            },
        },
        walking_sound =
        {
            {
                filename = "__base__/sound/walking/grass-01.ogg",
                volume = 0.8
            },
            {
                filename = "__base__/sound/walking/grass-02.ogg",
                volume = 0.8
            },
            {
                filename = "__base__/sound/walking/grass-03.ogg",
                volume = 0.8
            },
            {
                filename = "__base__/sound/walking/grass-04.ogg",
                volume = 0.8
            }
        },
        --map_color={r=160, g=160, b=160},
        map_color={r=0.63, g=0.63, b=0.63},
        ageing=0.0004,
        vehicle_friction_modifier = grass_vehicle_speed_modifier,
        pollution_absorption_per_second = 0.00001
    },

    {
        type = "tile",
        name = "surf-orig-sky-void",
        collision_mask =
        {
            "water-tile",
            "ground-tile",
            "item-layer",
            "resource-layer",
            "player-layer",
            "doodad-layer",
        },
        autoplace = make_autoplace_settings(100),
        layer = 80,
        needs_correction = false,
        variants =
        {
            main =
            {
                {
                    picture = "__Surfaces_Original__/graphics/terrain/sky/sky-void.png",
                    count = 2,
                    size = 1,
                },
            },
            inner_corner =
            {
                picture = "__Surfaces_Original__/graphics/terrain/sky/sky-void.png",
                count = 0
            },
            outer_corner =
            {
                picture = "__Surfaces_Original__/graphics/terrain/sky/sky-void.png",
                count = 0
            },
            side =
            {
                picture = "__Surfaces_Original__/graphics/terrain/sky/sky-void.png",
                count = 0
            },
            u_transition = 
            {
                picture = "__Surfaces_Original__/graphics/terrain/sky/sky-void.png",
                count = 0
            },
            o_transition = 
            {
                picture = "__Surfaces_Original__/graphics/terrain/sky/sky-void.png",
                count = 0
            },
        },
        allowed_neighbors = nil,
        map_color={r=0.2, g=0.175, b=0.15},
        ageing=0.0006,
        pollution_absorption_per_second = 0.0001
    },

    {
        type = "tile",
        name = "surf-orig-wood-tile",
        collision_mask =
        {
            "ground-tile",
        },
        autoplace = make_autoplace_settings(100),
        layer = 80,
        needs_correction = false,
        variants =
        {
            main =
            {
                {
                    picture = "__Surfaces_Original__/graphics/terrain/sky/wood.png",
                    count = 8,
                    size = 1,
                },
            },
            inner_corner =
            {
                picture = "__Surfaces_Original__/graphics/terrain/sky/wood.png",
                count = 0
            },
            outer_corner =
            {
                picture = "__Surfaces_Original__/graphics/terrain/sky/wood.png",
                count = 0
            },
            side =
            {
                picture = "__Surfaces_Original__/graphics/terrain/sky/wood.png",
                count = 0
            },
            u_transition = 
            {
                picture = "__Surfaces_Original__/graphics/terrain/sky/wood.png",
                count = 0
            },
            o_transition = 
            {
                picture = "__Surfaces_Original__/graphics/terrain/sky/wood.png",
                count = 0
            },
        },
        allowed_neighbors = nil,
        map_color={r=0.2, g=0.175, b=0.15},
        ageing=0.0006,
        pollution_absorption_per_second = 0.0001
    },

    {
        type = "tile",
        name = "surf-orig-wall",
        collision_mask =
        {
            "water-tile",
            "ground-tile",
            "item-layer",
            "resource-layer",
            "player-layer",
            "doodad-layer",
        },
        minable = {hardness = 0.2, mining_time = 5.0, results = {
          {
            amount_max = 2,
            amount_min = 0,
            name = "stone"
          }
        }},
        autoplace = make_autoplace_settings(100),
        layer = 80,
        needs_correction = false,
        variants =
        {
            main =
            {
                {
                    picture = "__Surfaces_Original__/graphics/terrain/cave-wall/cave1.png",
                    count = 16,
                    size = 1
                },
                {
                    picture = "__Surfaces_Original__/graphics/terrain/cave-wall/cave2.png",
                    count = 16,
                    size = 2
                },
                {
                    picture = "__Surfaces_Original__/graphics/terrain/cave-wall/cave4.png",
                    count = 16,
                    size = 4
                }
            },
            inner_corner =
            {
                picture = "__Surfaces_Original__/graphics/terrain/cave-wall/cave-inner-corner.png",
                count = 1
            },
            outer_corner =
            {
                picture = "__Surfaces_Original__/graphics/terrain/cave-wall/cave-outer-corner.png",
                count = 1
            },
            side =
            {
                picture = "__Surfaces_Original__/graphics/terrain/cave-wall/cave-side.png",
                count = 1
            },
            u_transition = 
            {
                picture = "__Surfaces_Original__/graphics/terrain/cave-wall/cave-u.png",
                count = 1
            },
            o_transition = 
            {
                picture = "__Surfaces_Original__/graphics/terrain/cave-wall/cave-o.png",
                count = 1
            },
        },
        allowed_neighbors = nil,
        map_color={r=0.2, g=0.175, b=0.15},
        ageing=0.0006,
        pollution_absorption_per_second = 0.0001
    },

    {
        type = "item",
        name = "explosive-landfill-surf-orig-ground",
        icon = "__Surfaces_Original__/graphics/icons/explosive-landfill.png",
        icon_size = 32,
        subgroup = "terrain",
        order = "a[explosive-landfill-cave-ground]",
        stack_size = 100,
        place_as_tile =
        {
            result = "surf-orig-ground",
            condition_size = 1,
            condition = { }
        }
    },

    {
        type = "recipe",
        name = "explosive-landfill-surf-orig-ground",
        enabled = "true",
        category = "crafting",
        ingredients =
        {
            {name = "explosives", amount = 1},
        },
        energy_required = 2,
        result = "explosive-landfill-surf-orig-ground",
        result_count = 5
    },

    {
        type = "item",
        name = "surf-orig-wooden-platform",
        icon = "__Surfaces_Original__/graphics/icons/wooden-platform.png",
        icon_size = 32,
        subgroup = "terrain",
        order = "a[wooden-platform]",
        stack_size = 100,
        place_as_tile =
        {
            result = "surf-orig-wood-tile",
            condition_size = 1,
            condition = { }
        }
    },

    {
        type = "recipe",
        name = "surf-orig-wooden-platform",
        category = "crafting",
        ingredients =
        {
            {name = "wood", amount = 1},
        },
        energy_required = 2,
        result = "surf-orig-wooden-platform",
        result_count = 10
    },

})

